FROM python:alpine3.6
RUN apk add --no-cache \
    build-base \
    postgresql-dev \
    libffi-dev \
    && pip install skygear[zmq] numpy \
    && apk del build-base libffi-dev \
    && mkdir -p /src
ENV CONSUL_HTTP_ADDR="https://ip-172-31-23-192:2087"
ONBUILD COPY . /src
WORKDIR /src
CMD [ "envconsul", "-prefix","cloud-functions","py-skygear" ]